# Ash Mirza - GitLab Exercise

import csv
import yaml
#import urllib2
from pprint import pprint

#read csv file function
def readCSV(file):
    gl_dept_group = []
    with open(file) as csvFile:
        csvReader = csv.reader(csvFile)
        for row in csvReader:
            gl_dept_group.append(row[0])
        return gl_dept_group

#use csv import function to get gl_dept_group list
gl_dept_group = readCSV('gl_dept_group-list.csv')

#add quotes to strings function
def add_quotes(a):
    b = "'" + a + "'"
    return b

#import stages.yml file with PyYAML
with open(r'stages.yml') as file:
    data = yaml.load(file, Loader=yaml.SafeLoader)
#import team.yml file with PyYAML
with open(r'team.yml') as file:
    team = yaml.load(file, Loader=yaml.SafeLoader)
#Jump to stages dictionary of YAML file, all parsing will be done at this level
data2 = data['stages']
# list of team member positions
manager_positions = ["backend_engineering_manager", "frontend_engineering_manager"]
team_positions = ["pm", "pmm", "cm", "support", "sets", "pdm", "ux", "uxr", "tech_writer", "tw_backup", "appsec_engineer"]


#iterate through yaml dict/list starting
for i in data2.keys():
    #for each dictionary within stages store dicitonary name as stage_name
    stage_name = i
    for j in data2[i]['groups'].keys():
        #since we already know each stage has a dictionary called "groups" we can directly start parsing that
        #for each groups dictionary we parse we store the name of the dictionary as group_name
        group_name = j
        #start counter to see if we are on first iteration of loop to list group members
        count = 0
        pcount = 0
        flag = False
        #initialize arrays to hold member names
        member_names = []
        manager_names = []
        for k in data2[i]['groups'][j].keys():
            #print(k)
            #now looping through all groups looking for managers
            for tmgr in manager_positions:
                if tmgr == k:
                    position_key_mgr = data2[i]['groups'][j][k]                    
                    #if position is just a string just output directly
                    #use add_quotes function to add singular quotes to final output
                    manager_names.append(str(position_key_mgr))
                    #if count is at 0 start header of output to match exercise requirement
                    if count == 0:
                        print("[\n'group_name' => '" + stage_name + "-" + group_name + "'")
                        print("'group_managers' => [")
                        flag = True
                    #counter increment
                    count = count+1
        #print manager names array            
        for n in range(len(manager_names)):
            print("'manager_name' => '" + manager_names[n] + "'")
            for y in range(len(team)):
                #manager name to gitlab handle lookup
                if str(manager_names[n]) == team[y]['name']:
                    if 'gitlab' in team[y].keys():
                        #print gitlab handle
                        print("'member_handle' => '" + team[y]['gitlab'] + "'")
        if manager_names:
            print('\t],') 
        
        for k in data2[i]['groups'][j].keys():
            for tpos in team_positions:
                if tpos == k:
                    position_key_pos = data2[i]['groups'][j][k]
                    #if position is a list/array iterate through list/array to list group members
                    if isinstance(position_key_pos, list):
                        for x in range(len(position_key_pos)):
                            member_names.append(str(position_key_pos[x]))  
                            fin_out_pos = add_quotes(str(position_key_pos[x]))
                            if pcount == 0:
                                if flag == False:
                                    print("[\n'group_name' => '" + stage_name + "-" + group_name + "'")
                                print("'group_members' => [")
                            pcount = pcount+1
                    else:
                        #if position is just a string just output directly
                        #use add_quotes function to add singular quotes to final output
                        fin_out_pos = add_quotes(str(position_key_pos))
                        member_names.append(str(position_key_pos))
                        if pcount == 0:
                            if flag == False:
                                print("[\n'group_name' => '" + stage_name + "-" + group_name + "'")
                            print("'group_members' => [")
                        pcount = pcount+1
        for n in range(len(member_names)):
            print("'member_name' = > '" + member_names[n] + "'")
            for y in range(len(team)):
                if str(member_names[n]) == team[y]['name']:
                    if 'gitlab' in team[y].keys():
                        print("'member_handle' => '" + team[y]['gitlab'] + "'")       
        if member_names:
            print('\t],\n],\n')